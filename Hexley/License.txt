Hexley DarwinOS Mascot Copyright 2000 by Jon Hooper
All Rights Reserved.

If you want to mass produce the mascot on T-shirts, CDROM's, etc you need to
request permission in advance. In general, I require that the mascot be used
in an appropriate way. This means that it has to be something related to
DarwinOS and not expropriated as a company logo (though I will allow
companies with Darwin-based products to use it). If you are just using the
logo on a T-Shirt for a few friends or team members you don't have to
request permission.

NOTE: A full copy of this license can be found on Hexley's Website:
http://www.hexley.com/